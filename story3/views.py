from django.shortcuts import render

# Create your views here.
def homepage(request):
    return render(request, 'story3/homepage.html')

def blogs(request):
    return render(request, 'story3/blogs.html')

def about_me(request):
    return render(request, 'story3/profile.html')

def agustus17th(request):
    return render(request, 'story3/17-agustusan.html')